// ----------------------------------------------------------------------------------------
// Initialize API fields for AWS API Gateway
// ----------------------------------------------------------------------------------------
const APIGatewayURL = 'https://ar3idz0ny8.execute-api.us-east-2.amazonaws.com/default/get-coordinates-test'
const prototypeURL = "https://18jof7a8y6.execute-api.us-east-1.amazonaws.com/dev/get-coordinates"
const APIKey = "krsZdIM1mc6WhYuCjhL2L48n7SXaYrnR8c2wcboQ"
const protoAPIKey ="NQKzmbkg7p9oMMlLvSuQU7de1OTtZdjT4Me1WIOY"

// ----------------------------------------------------------------------------------------
// Declare constants
// ----------------------------------------------------------------------------------------

const metertoPixel = 51.7
const pixeltoMeter = 0.01934235977
const raspPiLogoURL = "https://i.ibb.co/b3cqLZw/RPi-Logo-Reg-SCREEN.png"
// const crosshairsIconURL = 'https://i.ibb.co/Sff6fQj/cross-shaped-target.png'
const crosshairsIconURL = 'https://i.ibb.co/2M4Zp2r/cross-shaped-target-1.png'

const R2D2x = 0.29
const R2D2y = 10.29

const ARTix = 15.13
const ARTiy = 10.31

const TARSx = 15.13
const TARSy = 0.39

// ----------------------------------------------------------------------------------------
// Declare variables
// ----------------------------------------------------------------------------------------
// Declare SVG document
var draw = null
// Declare target SVG.text element
var target = null

var drawInterval = null
var r2d2circle = null
var articircle = null
var tarscircle = null
var showCircles = true
// Declare interval for updating target location
var updateTimer = null
// Represents target path
var pathPolyline = {
  // Array of target location history (x,y)
  points: [],
  // Path SVG.polyline element
  polylineDrawing: null,
  // Boolean representing state of Show Path button
  showPath: false
}
// Declare SVG.line elements for cursor click drawing
var crosshairs = {
  image: null,
  label: null,
  labelBackground: null,
  distance: function(x1,y1,x2,y2) { 
    return (Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))).toFixed(2) + "m\n"
  },
  draw: function(event){
    // Note: origin is top left
    const x = event.clientX 
    const y = event.clientY
    const xMeter = x * pixeltoMeter
    const yMeter = y * pixeltoMeter
    if (this.image != null){
      this.image.remove()
    } if (this.label != null){
      this.label.remove()
    } if (this.labelBackground != null){
      this.labelBackground.remove()
    }
    this.image = draw.image(crosshairsIconURL).size(50,50).move(x-27,y-27)
    const coordText = "X: " + (xMeter).toFixed(2) + "m, Y: " + (yMeter).toFixed(2) +"m\n"
    const disttoR2D2 = "Dist to R2D2: " + this.distance(xMeter,yMeter, R2D2x, R2D2y)
    const disttoARTi = "Dist to ARTi: " + this.distance(xMeter,yMeter, ARTix, ARTiy)
    const disttoTARS = "Dist to TARS: " + this.distance(xMeter,yMeter, TARSx, TARSy)
    const dispText = coordText + disttoR2D2 + disttoARTi + disttoTARS
    console.log(dispText)
    // Draw text by crosshairs
    this.labelBackground = draw.rect(160,90).move(x+30,y-100).radius(10).fill('white')
    this.label = draw.text(dispText).move(x+31,y-98)
    this.label.attr('fill','black')
  }
}

// Calculates distance between two clicked points on map, draws polyline
var distance = {
  secondClick: false,
  distanceLine: null,
  distanceVal: null,
  srcCoords: null,
  draw: function(event) {
    // Populate Clicked Coordinates Info Alert
    if (this.secondClick) {
      var srcX = this.srcCoords[0]
      var srcY = this.srcCoords[1]
      var destX = event.clientX
      var destY = event.clientY
      console.log(srcX, srcY, destX, destY)
      this.distanceLine = draw.line(srcX, srcY, destX, destY).stroke({width:5, color: '#f06'})
      this.distanceVal = (Math.sqrt(Math.pow(destX - srcX, 2) + Math.pow(destY - srcY, 2)) * pixeltoMeter).toFixed(2)
      $("#clickedCoords").html("<strong>Calculated Distance: </strong>" + this.distanceVal + " m")
      $("#clickedCoords").css("visibility","visible")
    } else {
      $("#clickedCoords").css("visibility","hidden")
      this.srcCoords = [event.clientX, event.clientY]
      if (this.distanceLine != null){
        this.distanceLine.remove()
      }
    }

    this.secondClick = !(this.secondClick)
  }
}


// ----------------------------------------------------------------------------------------
// Define event listener functions
// ----------------------------------------------------------------------------------------
//  Initializes SVG document when page is loaded, starts tracking of target
SVG.on(document, 'DOMContentLoaded', function() {
  draw = SVG().addTo("#map").size('100%', '100%')
  draw.click(function(event){
    crosshairs.draw(event)
    distance.draw(event)
  })
  // Draw Raspberri Pi's in respective corners
  var Arti = draw.image(raspPiLogoURL).size(20,20).move(770, 520)
  var ArtiLabel = draw.text("Arti").move(740, 520).attr("fill","white")
  var R2D2 = draw.image(raspPiLogoURL).size(20,20).move(5, 520)
  var R2D2Label = draw.text("R2D2").move(25, 520).attr("fill","white")
  var TARS = draw.image(raspPiLogoURL).size(20,20).move(770, 5)
  var TARSLabel = draw.text("TARS").move(730, 5).attr("fill","white")
  var entranceLabel = draw.text("Room Entrance").move(5, 5).attr("fill","white")
  if ($("#liveTrackingSwitch").is(":checked")){
    startTracking()
  }
  
})

// // Handles click of Pause/Resume Tracking Button
// $("#toggleSync").click(function(){
//   // If Pause Tracking is clicked, pause tracking and switch button to "Resume Tracking"
// if ($(this).hasClass("btn-danger")) {
//   stopTracking()
//   $(this).removeClass("btn-danger")
//   $(this).addClass("btn-success")
//   $(this).text("Resume Tracking")
// }
// // If Resume Tracking is clicked, resume tracking and switch button to "Stop Tracking" 
// else {
//   startTracking()
//   $(this).removeClass("btn-success")
//   $(this).addClass("btn-danger")
//   $(this).text("Pause Tracking")
// }
// })

$("#drawCoords").click(function(){
  var event = {
    clientX: $("#inputX").val() * metertoPixel,
    clientY: $("#inputY").val() * metertoPixel
  }
  crosshairs.draw(event)
  distance.draw(event)
  $("#inputX").val("")
  $("#inputY").val("")

})

$("#cancelTimeDraw").click(function(){
  clearInterval(drawInterval)
})

// Handles click of Show/Hide Path Button
$("#togglePath").click(function(){
    // If Hide Path is clicked, hide path and switch button to "Show Path"
  if ($(this).hasClass("btn-danger")) {
    pathPolyline.showPath = false
    if (pathPolyline.polylineDrawing != null){
    pathPolyline.polylineDrawing.remove()
    }
    $(this).removeClass("btn-danger")
    $(this).addClass("btn-success")
    $(this).text("Show Path")
  } 
    // If Show Path is clicked, show path and switch button to "Hide Path"
  else {
    pathPolyline.showPath = true
    $(this).removeClass("btn-success")
    $(this).addClass("btn-danger")
    $(this).text("Hide Path")
  }
  })

  // Handles click of Reset Path Button
$("#resetPath").click(function(){
  pathPolyline.points = []
})

$("#toggleCircles").click(function(){
  // If Pause Tracking is clicked, pause tracking and switch button to "Resume Tracking"
if ($(this).hasClass("btn-danger")) {
  showCircles = false
  $(this).removeClass("btn-danger")
  $(this).addClass("btn-success")
  $(this).text("Show Circles")
}
// If Resume Tracking is clicked, resume tracking and switch button to "Stop Tracking" 
else {
  showCircles = true
  $(this).removeClass("btn-success")
  $(this).addClass("btn-danger")
  $(this).text("Hide Circles")
}
})


// ----------------------------------------------------------------------------------------
// Define helper functions
// ----------------------------------------------------------------------------------------
// Sets interval for retreiving target coords and drawing target
function startTracking() {
  updateTimer = setInterval(liveTrack,1000)
  console.log("tracking started")
}

// Stops interval for retreiving target coords and drawing target
function stopTracking() {
  clearInterval(updateTimer)
  console.log("tracking stopped")
}

function liveTrack(){
  {
    var request = new XMLHttpRequest()
    // Open a new connection, using the GET request on the URL endpoint

    var params = {
      firstTime: "",
      secondTime: ""
      }
      request.open('GET', prototypeURL + "?" + $.param(params), true)
    request.setRequestHeader('Content-Type', 'application/json')
    request.setRequestHeader("x-api-key",protoAPIKey)
    request.onload = function() {
      // Begin accessing JSON data here
        var data = JSON.parse(this.response)
    // If request is successful, draw target
      if (request.status >= 200 && request.status < 400) {
        // Read out x and y coords
        let retreivedData = (JSON.parse(data.body)).Success
        console.log("retreived data", retreivedData)
        let x = retreivedData[0] * metertoPixel 
        let y = retreivedData[1] * metertoPixel
        let r2d2Radius = retreivedData[3] * metertoPixel
        let artiRadius = retreivedData[4] * metertoPixel
        let tarsRadius = retreivedData[5] * metertoPixel
        drawSinglePoint(x,y, r2d2Radius, artiRadius, tarsRadius)
      } else {
        console.log('error')
      }
    }
    request.send()
    }

}

function drawSinglePoint(x, y, r2d2Radius, artiRadius, tarsRadius) {
    // Save coordinates to path array 
    pathPolyline.points.push(x, y)
    // Draw target for first time
    if (target == null){
    target = draw.text("📱")
    target.font({
      size: 32
    })
    target.move(x-19, y-24)
    
    if (showCircles) {
      r2d2circle = draw.circle(r2d2Radius * 2).fill('none').cx(R2D2x * metertoPixel).cy(R2D2y * metertoPixel)
      r2d2circle.stroke({color:'white', width: 5}) 

      articircle = draw.circle(artiRadius * 2).fill('none').cx(ARTix * metertoPixel).cy(ARTiy * metertoPixel)
      articircle.stroke({color:'blue', width: 5}) 

      tarscircle = draw.circle(tarsRadius * 2).fill('none').cx(TARSx * metertoPixel).cy(TARSy * metertoPixel)
      tarscircle.stroke({color:'green', width: 5}) 
    }
  } 
    // Move target to new location with animation
    else {
      target.animate().move(x-19, y-24)
      if (showCircles) {
        if (r2d2circle != null)
          r2d2circle.animate().attr({r:r2d2Radius})
        else {
          r2d2circle = draw.circle(r2d2Radius * 2).fill('none').cx(R2D2x * metertoPixel).cy(R2D2y * metertoPixel)
          r2d2circle.stroke({color:'white', width: 5}) 
        }
        if (articircle != null)
          articircle.animate().attr({r:artiRadius})
        else {
          articircle = draw.circle(artiRadius * 2).fill('none').cx(ARTix * metertoPixel).cy(ARTiy * metertoPixel)
          articircle.stroke({color:'blue', width: 5}) 
        }
        if (tarscircle != null)
          tarscircle.animate().attr({r:tarsRadius})
        else {
          tarscircle = draw.circle(tarsRadius * 2).fill('none').cx(TARSx * metertoPixel).cy(TARSy * metertoPixel)
          tarscircle.stroke({color:'green', width: 5}) 
        }
    } else {
      r2d2circle.remove()
      articircle.remove()
      tarscircle.remove()
      r2d2circle = null
      articircle = null
      tarscircle = null
    }
  }

    // Remove existing path
    if (pathPolyline.polylineDrawing != null){
      pathPolyline.polylineDrawing.remove()
    }
    // Draw target path if Show Path button was clicked
    if (pathPolyline.showPath){
    pathPolyline.polylineDrawing = draw.polyline(pathPolyline.points).fill('none')
    pathPolyline.polylineDrawing.stroke({color: '#f06', width: 4, linecap: 'round', linejoin: 'round'})
    } 
    
} 



// Draws phone on map at each coordinate in list, with one second delay between each point
function drawfromList(pointlist) {
    // PointList Format:
    // List of Lists
    // Each element: [x, y, time, r2d2_r, arti_r, tars_r]
    var i = 0
    var pointListLen = pointlist.length
    drawInterval = setInterval(function(){
      if (i < pointListLen) {
        let x = pointlist[i][0] * metertoPixel 
        let y = pointlist[i][1] * metertoPixel
        let r2d2Radius = pointlist[i][3] * metertoPixel
        let artiRadius = pointlist[i][4] * metertoPixel
        let tarsRadius = pointlist[i][5] * metertoPixel
        drawSinglePoint(x, y, r2d2Radius, artiRadius, tarsRadius) 
        i++
      }
      else {
        clearInterval(drawInterval)
      }
    },1000)
}

$("#drawfromTime").click(function(){
  drawPoint($("#startDate").val().replace("T"," "),$("#endDate").val().replace("T"," "))

})

$("#liveTrackingSwitch").change(function() {
  if(this.checked) {
      startTracking()
  } else {
    stopTracking()
  }
})

// Makes GET request to AWS API Gateway Endpoint to retreive coordinates of target
// Draws phone emoji at retreived coordinates and allows for drawing target path
function drawPoint(startTime, endTime) {
var request = new XMLHttpRequest()
// Open a new connection, using the GET request on the URL endpoint
var params = {
firstTime: startTime,
secondTime: endTime
}
request.open('GET', prototypeURL + "?" + $.param(params), true)
request.setRequestHeader('Content-Type', 'application/json')
request.setRequestHeader("x-api-key",protoAPIKey)
request.onload = function() {
  // Begin accessing JSON data here
    var data = JSON.parse(this.response)
// If request is successful, draw target
  if (request.status >= 200 && request.status < 400) {
    // Read out x and y coords
    let retreivedData = (JSON.parse(data.body)).Success
    console.log("retreived data", this.response)
    drawfromList(retreivedData)
  } else {
    console.log('error')
  }
}
request.send()
}

// Adds a regular clock
/* function GetClock(){
  var d=new Date();
  var nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getFullYear();
  var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;
  
  if(nhour==0){ap=" AM";nhour=12;}
  else if(nhour<12){ap=" AM";}
  else if(nhour==12){ap=" PM";}
  else if(nhour>12){ap=" PM";nhour-=12;}
  
  if(nmin<=9) nmin="0"+nmin;
  if(nsec<=9) nsec="0"+nsec;
  
  var clocktext=""+(nmonth+1)+"/"+ndate+"/"+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
  document.getElementById('clockbox').innerHTML=clocktext;
  }
  
  GetClock();
  setInterval(GetClock,1000); */
